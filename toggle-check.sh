#!/bin/sh

for name in "$@"
do
	name="`basename "$name"`"
		
	case $name in
		'✔'*) mv "$name" "${name#✔ }" ;;
		*) mv "$name" "✔ $name" ;;
	esac
done
